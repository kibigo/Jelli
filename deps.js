// 🎐👾 Jelli Game Engine ∷ deps.js
// ====================================================================
//
// Copyright © 2022 Margaret KIBI.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

export {
  asciiLowercase,
  isIntegerIndex,
  splitOnASCIIWhitespace,
  stripAndCollapseASCIIWhitespace,
  toIndex,
} from "https://gitlab.com/kibigo/Pisces/-/raw/35268a75dd34f5673edc920a5919643ad99ef092/mod.js";
export { default as Lemon } from "https://gitlab.com/kibigo/Lemon/-/raw/521e799db7b9f4cb2a18a8e80d035fba994c77f3/mod.js";
