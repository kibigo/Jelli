// 🎐👾 Jelli Game Engine ∷ Elements/jelli-screen.js
// ====================================================================
//
// Copyright © 2022 Margaret KIBI.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

import {
  asciiLowercase,
  isIntegerIndex,
  Lemon,
  splitOnASCIIWhitespace,
  stripAndCollapseASCIIWhitespace,
  toIndex,
} from "../deps.js";

{ // Cheaply polyfill OffscreenCanvas.
  //
  // Remove this once Safari supports it natively.
  globalThis.OffscreenCanvas ??= class OffscreenCanvas
    extends HTMLCanvasElement {
    constructor(width, height) {
      return Lemon.canvas({ width, height })``;
    }
  };
  globalThis.OffscreenCanvasRenderingContext2D ??=
    CanvasRenderingContext2D;
}

{ // Cheaply polyfill CanvasRenderingContext2D::reset().
  //
  // Remove this once Firefox supports it natively.
  CanvasRenderingContext2D.prototype.reset ??= function () {
    const { canvas: { width, height } } = this;
    this.clearRect(0, 0, width, height);
  };
  OffscreenCanvasRenderingContext2D.prototype.reset ??= function () {
    const { canvas: { width, height } } = this;
    this.clearRect(0, 0, width, height);
  };
}

/**
 * An H·T·M·L element providing a canvas which composes a number of
 * drawable 2·D layers and records pointer events in the form of Pokes.
 */
export default class JelliScreenElement extends HTMLElement {
  /** The onscreen canvas element for this Screen. */
  #canvas;

  /** The 2·D rendering context used by this Screen. */
  #context;

  /** The default size of this Screen in a dimension. */
  #defaultDimension(horizontal = false) {
    const d = horizontal ? "DEFAULT_WIDTH" : "DEFAULT_HEIGHT";
    return (this.constructor?.[d] ?? JelliScreenElement[d]) >>> 0 || 1;
  }

  /**
   * Handles pointer events and uses them to create, delete, and update
   * the Pokes associated with this Screen.
   */
  #handlePointerEvent(event) {
    const pokes = this.#pokes;
    const { pointerId } = event;
    switch (event.type) {
      case "pointercancel":
      case "pointerup": {
        if (pokes.has(pointerId)) {
          // The pointer corresponding to the event is registered as a
          // Poke.
          this.#canvas.releasePointerCapture(pointerId);
          delete pokes.ordered[pokes.get(pointerId).index];
          pokes.delete(pointerId);
          event.preventDefault();
          break;
        } else {
          // The pointer corresponding to the event is not registered.
          break;
        }
      }
      case "pointerdown": {
        if (pokes.has(pointerId)) {
          // The pointer corresponding to the event is already
          // registered as a Poke.
          Reflect.apply(
            pokeBehaviours.updateWith,
            pokes.get(pointerId),
            [event],
          );
          event.preventDefault();
          break;
        } else if (event.buttons & 1) {
          const { ordered } = pokes;
          const index = ordered.firstAvailableIndex;
          const poke = makePoke(event, this, index);
          if (
            poke.x >= 0 && poke.x <= this.width &&
            poke.y >= 0 && poke.y <= this.height
          ) {
            // The event is in‐bounds and can be handled.
            pokes.set(pointerId, ordered[index] = poke);
            this.#canvas.setPointerCapture(pointerId);
            event.preventDefault();
            break;
          } else {
            // The event is out‐of‐bounds.
            break;
          }
        } else {
          // The event was not in the primary button state.
          break;
        }
      }
      case "pointermove": {
        if (pokes.has(pointerId)) {
          // The pointer corresponding to the event is registered as a
          // Poke.
          Reflect.apply(
            pokeBehaviours.updateWith,
            pokes.get(pointerId),
            [event],
          );
          event.preventDefault();
          break;
        } else {
          // The pointer corresponding to the event is not registered.
          break;
        }
      }
    }
  }

  /** The Layers associated with this Screen. */
  #layers = new Layers();

  /** Paints the layers of this Screen into its context. */
  #paintLayers() {
    if (!this.isConnected) {
      // This element is not connected to a document.
      //
      // It isn’t necessary to cancel the request, as it is almost
      // certainly what triggered this method call.
      this.#request = undefined;
    } else {
      // This element is connected to a document and needs to be
      // re·rendered.
      const context = this.#context;
      context.reset();
      for (const layer of getCanvasMap(this.#layers).values()) {
        context.drawImage(layer, 0, 0);
      }
      this.#request = this.view?.requestAnimationFrame?.(
        this.#paintLayers.bind(this),
      );
    }
  }

  /**
   * A map associating pointer I·D’s with Pokes.
   *
   * The `ordered` property is an arraylike object which keeps track of
   * the “order” in which Pokes occur. Order does not necessarily
   * strictly increase over time; if a second Poke ends, a subsequent
   * Poke may be assigned index 2 even if index 3 is still active.
   */
  #pokes = Object.assign(new Map(), {
    ordered: Object.create(null, orderedPokesPropertyDescriptors),
  });

  /**
   * The animation frame request for updating this Screen, if one is
   * active.
   */
  #request;

  /** The scale factor of this Screen. */
  #scale = 1;

  /** Updates the size of this Screen in a dimension. */
  #updateDimension($, horizontal = false) {
    const canvas = this.#canvas;
    const defaultValue = this.#defaultDimension(horizontal);
    const dimension = Math.floor(Math.max(0, $));
    const orientation = horizontal ? "width" : "height";
    canvas[orientation] = isFinite(dimension)
      ? dimension || defaultValue
      : defaultValue;
    const newValue = canvas[orientation];
    for (const layer of getCanvasMap(this.#layers).values()) {
      layer[orientation] = newValue;
    }
    canvas.style[orientation] = `${
      Math.max(Math.floor(newValue * this.#scale), 1)
    }px`;
    return newValue;
  }

  /**
   * Updates the scale of this Screen and returns it.
   *
   * If the provided value is NaN, 1 will be used instead. Zero or
   * negative scales will be replaced with Number.MIN_VALUE.
   */
  #updateScale($) {
    const canvas = this.#canvas;
    const { width, height } = canvas;
    const scale = (() => {
      const n = $ === "" ? 1 : +$;
      return Number.isNaN(n)
        ? 1
        : Math.min(Math.max(Number.MIN_VALUE, n), Number.MAX_VALUE);
    })();
    Object.assign(canvas.style, {
      height: `${Math.max(Math.floor(height * scale), 1)}px`,
      width: `${Math.max(Math.floor(width * scale), 1)}px`,
    });
    this.#scale = scale;
    return scale;
  }

  /** Constructs a new Screen. */
  constructor() {
    super();
    const shadow = this.attachShadow({ mode: "closed" });
    const canvas = this.#canvas = Lemon.canvas({
      height: this.#defaultDimension(false),
      width: this.#defaultDimension(true),
    })`${Lemon.slot``}`;
    this.#context = canvas.getContext("2d");
    canvas.width = this.#defaultDimension(true);
    canvas.height = this.#defaultDimension(false);
    Object.assign(canvas.style, {
      display: "block",
      margin: "auto",
      height: `${canvas.height}px`,
      width: `${canvas.width}px`,
      imageRendering: "pixelated",
      objectFit: "contain",
    });
    const handlePointerEvent = this.#handlePointerEvent.bind(this);
    canvas.addEventListener("pointerdown", handlePointerEvent);
    canvas.addEventListener("pointermove", handlePointerEvent);
    canvas.addEventListener("pointerup", handlePointerEvent);
    canvas.addEventListener("pointercancel", handlePointerEvent);
    shadow.append(
      Lemon.slot({ name: "preamble" })``,
      canvas,
      Lemon.slot({ name: "postamble" })``,
    );
  }

  /** The default height of a Screen. */
  static DEFAULT_HEIGHT = 288;

  /** The default width of a Screen. */
  static DEFAULT_WIDTH = 320;

  /** The attributes observed on Screen elements. */
  static get observedAttributes() {
    return ["height", "layers", "scale", "width"];
  }

  /** Reacts to the adoption of this Screen into a new document. */
  adoptedCallback(oldDoc) {
    { // Handle existing requests.
      const existingRequest = this.#request;
      if (existingRequest != null) {
        // There was an existing request open on the old document.
        oldDoc?.defaultView?.cancelAnimationFrame?.(existingRequest);
      } else {
        // No existing request was open.
        /* do nothing */
      }
    }
    { // Open a new paint request if applicable.
      if (this.isConnected) {
        // This screen is connected to the new document; queue up a new
        // paint request.
        this.#request ??= this.view?.requestAnimationFrame?.(
          this.#paintLayers.bind(this),
        );
      } else {
        // This screen is not connected.
        /* do nothing */
      }
    }
  }

  /** Reacts to changes to the observed attributes on this Screen. */
  attributeChangedCallback(name, _oldValue, newValue, namespace) {
    if (namespace) {
      // The attribute is namespaced.
      //
      // ※ This shouldn’t be possible.
      /* do nothing */
    } else {
      // The attribute is not namespaced.
      switch (name) {
        case "height": {
          // The @height attribute changed.
          this.#updateDimension(newValue, false);
          break;
        }
        case "layers": {
          // The @layers attribute changed.
          const { width, height } = this.#canvas;
          const newCanvasMap = new Map();
          const oldCanvasMap = getCanvasMap(this.#layers);
          const layerNames = stripAndCollapseASCIIWhitespace(newValue);
          for (const givenName of splitOnASCIIWhitespace(layerNames)) {
            const name = asciiLowercase(givenName);
            if (newCanvasMap.has(name)) {
              /* do nothing */
            } else if (oldCanvasMap.has(name)) {
              newCanvasMap.set(name, oldCanvasMap.get(name));
            } else {
              const canvas = new OffscreenCanvas(width, height);
              canvas.getContext("2d");
              newCanvasMap.set(name, canvas);
            }
          }
          setCanvasMap(this.#layers, newCanvasMap);
          break;
        }
        case "scale": {
          // The @scale attribute changed.
          this.#updateScale(newValue);
          break;
        }
        case "width": {
          // The @width attribute changed.
          this.#updateDimension(newValue, true);
          break;
        }
      }
    }
  }

  /**
   * Handles connecting this Screen to a document by queueing up a
   * paint.
   */
  connectedCallback() {
    if (this.isConnected) {
      this.#request ??= this.ownerDocument?.defaultView
        ?.requestAnimationFrame?.(this.#paintLayers.bind(this));
    }
  }

  /** Gets and sets the height of this Screen, in canvas units. */
  get height() {
    return this.#canvas.height;
  }
  set height(n) {
    this.setAttribute("height", this.#updateDimension(n, false));
  }

  /**
   * Gets an object whose `names` and `contexts` methods iterate over
   * the available layers in this Screen, and whose `layer` method can
   * be used to access a specific layer by name or index.
   */
  get layers() {
    return this.#layers;
  }

  /**
   * Gets a (potentially sparse, nonlive) array of Pokes currently
   * active on this Screen.
   *
   * Pokes are inserted into the result array by their `index`, which
   * gives the lowest nonnegative integer not assigned to an existing
   * Poke on this Screen at time of creation. Once set, the `index` of
   * a Poke (and consequently its position within the `pokes` array)
   * will not change.
   */
  get pokes() {
    return Reflect.apply(
      Array.prototype.slice,
      this.#pokes.ordered,
      [],
    );
  }

  /**
   * Resets each layer in this Screen.
   *
   * This will result in a blank canvas on the next paint.
   */
  reset() {
    for (const context of this.#layers.contexts()) {
      context.reset();
    }
  }

  /** Gets and sets the rendered scale of this Screen. */
  get scale() {
    return this.#scale;
  }
  set scale(n) {
    this.setAttribute("scale", this.#updateScale(n));
  }

  /**
   * Gets the Window associated with this Screen, or undefined if it is
   * not connected to a document with a `defaultView`.
   */
  get view() {
    return this.ownerDocument?.defaultView ?? undefined;
  }

  /** Gets and sets the width of this Screen, in canvas units. */
  get width() {
    return this.#canvas.width;
  }
  set width(n) {
    this.setAttribute("width", this.#updateDimension(n, true));
  }
}

const { Layers, getCanvasMap, setCanvasMap } = (() => {
  let getCanvasMap;
  let setCanvasMap;

  /**
   * A wrapper for a mapping of names to canvases which provides
   * iterators for their contexts.
   *
   * It is possible to get the original canvases from the contexts
   * using their `canvas` properties; this doesn’t prevent that!
   *
   * The constructor for this class is not made available outside of
   * this module, but its prototype is.
   */
  class Layers {
    #canvasMap;

    /** Constructs a new Layers object with the provided canvas map. */
    constructor(map = new Map()) {
      this.#canvasMap = map;
    }

    static { // provide getters and setters for $.#canvasMap
      getCanvasMap = ($) => $.#canvasMap;
      setCanvasMap = ($, map) => $.#canvasMap = map;
    }

    /** Yields the contexts in this Layers object. */
    *contexts() {
      for (const layer of this.#canvasMap.values()) {
        yield layer.getContext("2d");
      }
    }

    /**
     * Returns the layer in this Layers object corresponding to the
     * provided name or integer index, or undefined if none exists.
     *
     * It is not possible to access by name layers whose names are also
     * integer indices.
     */
    layer(n) {
      const map = this.#canvasMap;
      if (isIntegerIndex(n)) {
        // The provided value is an integer index.
        const index = toIndex(n);
        if (index >= map.size) {
          // The index exceeds the size of the map.
          return undefined;
        } else {
          // The index is smaller than the size of the map.
          const layerIterator = map.values();
          let result;
          let done = false;
          let read = 0;
          do {
            result = layerIterator.next();
            done = result.done;
          } while (!done && ++read <= index);
          if (done) {
            // The map was exhausted before reaching the provided
            // value.
            //
            // ※ This shouldn’t be possible.
            return undefined;
          } else {
            // A result for the given index was found.
            return result.getContext("2d");
          }
        }
      } else if (map.has(n)) {
        // The provided value is a name corresponding to a layer.
        return map.get(n).getContext("2d");
      } else {
        // The provided value is neither an integer index nor a known
        // name.
        return undefined;
      }
    }

    /** Yields the names in this Layers object. */
    *names() {
      for (const name of this.#canvasMap.keys()) {
        yield name;
      }
    }

    /** Gets the size of this Layers object. */
    get size() {
      return this.#canvasMap.size;
    }

    /** Yields the name~object pairs in this Layers object. */
    *[Symbol.iterator]() {
      for (const [name, layer] of this.#canvasMap.entries()) {
        yield [name, layer.getContext("2d")];
      }
    }
  }

  delete Layers.prototype.constructor;
  return { Layers, getCanvasMap, setCanvasMap };
})();

/**
 * A pointer event of some kind.
 *
 * This class cannot be constructed. Its behaviours are provided via an
 * auxilliary class which is not exposed to users; attempting to call
 * Poke prototype methods on user·created objects will always throw an
 * error.
 */
export class Poke {
  /** Throws a TypeError. */
  constructor() {
    throw new TypeError("Jelli: Illegal constructor.");
  }

  /**
   * Gets the pointer I·D associated with this Poke.
   *
   * This value will always match `event.pointerId`.
   */
  get id() {
    return Reflect.get(pokeBehaviours, "id", this);
  }

  /**
   * Gets the index of this Poke.
   *
   * This is the lowest nonnegative integer for which no Poke with the
   * same `target` as this one already exists.
   */
  get index() {
    return Reflect.get(pokeBehaviours, "index", this);
  }

  /** Gets the last PointerEvent which was used to update this Poke. */
  get lastEvent() {
    return Reflect.get(pokeBehaviours, "lastEvent", this);
  }

  /** Gets the timestamp of the most recent update to this Poke. */
  get lastUpdated() {
    return Reflect.get(pokeBehaviours, "lastUpdated", this);
  }

  /** Gets the JelliScreenElement which this Poke is registered to. */
  get target() {
    return Reflect.get(pokeBehaviours, "target", this);
  }

  /** Gets the X‐co·ordinate of this Poke, in screen units. */
  get x() {
    return Reflect.get(pokeBehaviours, "x", this);
  }

  /** Gets the Y‐co·ordinate of this Poke, in screen units. */
  get y() {
    return Reflect.get(pokeBehaviours, "y", this);
  }
}

const { makePoke, pokeBehaviours } = (() => {
  /**
   * A Poke with behaviours available.
   *
   * This class is not exposed to users. It provides the behaviours
   * associated with Poke instances via private features.
   */
  class PokeWithBehaviours extends function ($) {
    return $; // make whatever is passed to `super` into `this`
  } {
    #index;
    #lastEvent;
    #pointerId;
    #target;
    #timeStamp;
    #x;
    #y;

    /**
     * Constructs a new Poke (not PokeWithBehaviours) which has the
     * necessary private features for PokeWithBehaviour prototype
     * methods and property access.
     */
    constructor(event, target, index) {
      super(Object.create(Poke.prototype));
      const box = event.currentTarget.getBoundingClientRect();
      this.#index = index;
      this.#lastEvent = event;
      this.#pointerId = event.pointerId;
      this.#target = target;
      this.#timeStamp = event.timeStamp;
      this.#x = (event.clientX - box.x) * (target.width / box.width);
      this.#y = (event.clientY - box.y) * (target.height / box.height);
    }

    /** Gets the pointer I·D associated with this object. */
    get id() {
      return this.#pointerId;
    }

    /** Gets the index associated with this object. */
    get index() {
      return this.#index;
    }

    /** Gets the last event associated with this object. */
    get lastEvent() {
      return this.#lastEvent;
    }

    /** Gets the timestamp associated with this object. */
    get lastUpdated() {
      return this.#timeStamp;
    }

    /** Gets the target associated with this object. */
    get target() {
      return this.#target;
    }

    /**
     * Updates the private features of this object using the provided
     * event.
     */
    updateWith(event) {
      if (this.#pointerId !== event.pointerId) {
        throw new TypeError("Jelli: Poke id mismatch.");
      } else {
        const target = this.#target;
        const { clientX, clientY, currentTarget, timeStamp } = event;
        const box = currentTarget.getBoundingClientRect();
        const x = (clientX - box.x) * (target.width / box.width);
        const y = (clientY - box.y) * (target.height / box.height);
        this.#lastEvent = event;
        this.#x = x;
        this.#y = y;
        this.#timeStamp = timeStamp;
      }
    }

    /** Gets the X‐co·ordinate associated with this object. */
    get x() {
      return this.#x;
    }

    /** Gets the Y‐co·ordinate associated with this object. */
    get y() {
      return this.#y;
    }
  }

  return {
    makePoke: (...args) => new PokeWithBehaviours(...args),
    pokeBehaviours: PokeWithBehaviours.prototype,
  };
})();

/**
 * Property descriptors for the `ordered` property of Screen instance
 * internal Poke maps.
 */
const orderedPokesPropertyDescriptors = {
  /** Gets the first available index in this object. */
  firstAvailableIndex: {
    configurable: false,
    enumerable: false,
    get: function () {
      for (
        let index = 0;
        index < Number.MAX_SAFE_INTEGER;
        ++index
      ) {
        if (!(index in this)) {
          // The index is currently not assigned on this object.
          return index;
        } else {
          // The index is currently assigned.
          continue;
        }
      }
      throw new RangeError("Jelli: Poke size exceeded.");
    },
  },

  /**
   * Gets the greatest index in this object and returns a number one
   * greater.
   *
   * The implementation of `length` is built on the assumption that
   * this object only contains integer indices for its enumerable
   * properties.
   */
  length: {
    configurable: false,
    enumerable: false,
    get: function () {
      return +Object.keys(this).pop() + 1;
    },
  },

  /**
   * Iterates over the values corresponding to the integer indices of
   * this object.
   */
  [Symbol.iterator]: {
    configurable: false,
    enumerable: false,
    value: Array.prototype[Symbol.iterator],
    writable: false,
  },
};

customElements.define("jelli-screen", JelliScreenElement);
